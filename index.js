// console.log("TGIF!");

// What are CONDITIONAL STATEMENTS?
	// It allows us to control the flow of our program
	// It also allows us to run statement or instruction if a condition is met or run another separate instruction if otherwise.

// [Section] If, Else if, and Else statement

	let numA = -1;

// If statement
		// It will execute the statement if a specified condition is met or true.
		// The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.

		/*Syntax:
		if(condition){
			statement;
		}*/

	/*Example*/
	if(numA<0){
		console.log("Hello"); /*ans: true, the statement will be rendered*/
	}
	console.log(numA<0); /*ans: true*/
	
	/*Example*/
	numA = 0;
	if(numA<0){
		console.log("Hello again in numA is 0"); /*ans: false, will not render*/
	}
	console.log(numA<0); /*ans: false*/
	// It will not run because the expression now results to false.

	/*Example*/
	let city = "New York";
	if(city === "New York"){
		console.log("Welcome to New York!"); /*ans: true, will render*/
	}


// Else if clause
		// Executes a statement if previous conditions are false and if the specified condition is true
		// The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.

	/*Example*/
	let numH = 1;
	if(numH<0){
		console.log("Hello from numH"); /*ans: false*/
	}
	else if(numH>0){
		console.log("Hi. I'm numH!"); /*ans: true*/
	}
	// We were able to run the else if() statement after we evaluate that the if() condition was failed or false
	// If the if() condition was passed and run, we will no longer evaluate to else if() and will end the process there.

	/*Example*/
	if(numH>0){
		console.log("Hello from numH!"); /*ans: true*/
	}
	else if(numH === 1){
		console.log("Hi I'm the second condition met!"); /*ans: true*/
	}
	else if(numH<0){
		console.log("Hi I'm numH!"); /*ans: false*/
	}
	console.log(numH>0) /*ans: true*/
	console.log(numH === 1) /*ans: true*/
	console.log(numH<0) /*ans: false*/
	// Basta may maunang mag run na true, hindi na marerender ang ibang true.
	// Else if() statement was not executed because the if() statement was able to run, and the evaluation of the whole statement stops there.

	/*Example*/
	// Let's update the city variable and look at another example
	city = "Tokyo";

	if(city === "New York"){
		console.log("Welcome to New York, USA!"); /*ans: false*/
	}
	else if(city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan!"); /*ans: true, will render*/
	}


// Else statement
		// Executes a statement if all other conditions are false or not met
		// Else statement is optional and can be added to capture any other result to change the flow of a program.

	numH = 2;

	/*Example*/
	if(numH<0){
		console.log("Hello I'm numH"); /*ans: false*/
	}
	else if(numH>2){
		console.log("numH is greater than 2"); /*ans: false*/
	}
	else if(numH>3){
		console.log("numH is greater than 3"); /*ans: false*/
	}
	else{
		console.log("numH from else"); /*ans: will be rendered because it is the last choice*/
	}

	// Since all the preceding if() and else if() conditions failed, the else{} statement will be ran instead
	// Else and else if statements should only be added if there is a preceeding if condition, else, statements by itself will not work. However, if statements will work even if there is no else statement.


// [Section] If, Else if, and Else statement with Functions
	// Most of the time, we would like to use if, else if, and else statements with functions to control the flow of our application.
	// By including them inside the functions, we can decide when certain conditions will be checked instead of executing statements when the Javascript loads
	// The "return" statements can also be used. It can also be utilized with conditional statements in combination with functions to change values to be used for other features of our application.

	let message;

	function determineTyphoonIntensity(windSpeed){
		if(windSpeed<0){
			return "Invalid Argument.";
		}
		else if(windSpeed>0 && windSpeed<31){
			return "Not a typhoon yet.";
		}
		else if(windSpeed<=60){
			return "Tropical depression detected.";
		}
		else if(windSpeed>=61 && windSpeed<=88){
			return "Tropical storm detected.";
		}
		else if(windSpeed>=89 && windSpeed<=117){
			return "Severe tropical storm detected.";
		}
		else{
			return "Typhoon detected.";
		}
	}
	
	// Returns the string to the variable message that invoked.
	message = determineTyphoonIntensity(123);
	console.log(message);

	// We can further control the flow of our program based on conditions and changing variables and results.
	// Due to the conditional statements created in the situation, we were able to reassign its value and its new value to print different output

	// console.warn() - good way to print warnings in our console that could help us act on certain output within our code.
	if(message === "Typhoon detected."){
		console.warn(message);
	}


// [Section] Truthy and Falsy

	/*
	In Javascript, a "truthy" value is a value that is considered true when encountered in a Boolean context.
	- Values are considered true unless defined otherwise.

	"Falsy" values o exceptions for truthy:
		1. false
    	2. 0
    	3. -0
    	4. ""
    	5. null
    	6. undefined
    	7. NaN (not a number)
	*/

// Truthy Examples

	if(true){
		console.log("Truthy"); /*will be rendered*/
	}

	if(1){
		console.log("Truthy"); /*will be rendered*/
	}

	if([]){
		console.log("Truthy"); /*will be rendered*/
	}

// Falsy Examples

	if(false){
		console.log("Falsy"); /*will not run*/
	}

	if(0){
		console.log("Falsy"); /*will not run*/
	}

	if(undefined){
		console.log("Falsy"); /*will not run*/
	}


// [Section] Conditional (Ternary) Operator

/*
	Conditional Ternary Operator
	1. Condition
	2. Expression to execute if the condition is truthy
	3. Expression if the condition is falsy

	- It can be used as an alternative to an "if else" statement
	- Ternary operators have an implicit "return" statement. Meaning, without return keyword.
	- The resulting expression can be stored in a variable.
	- Commonly used for single statement execution where result consists of only one line of code.

	Syntax:
		(expression) ? ifTrue : ifFalse;
*/

	// Single Statement Execution
		
		let ternaryResult = (1<18) ? true : false;
		console.log("Result of Ternary Operator: " + ternaryResult)
		
		/*ans: true*/
		/*or let ternaryResult = (1<18) ? 1 : 2;    ans: 1*/


	// Multiple Statement Execution
		// Both funtions perform two separate tasks which changes the value of the "name" variable and returns the result storing it in the "legalAge" variable.

		let name;
		function isOfLegalAge(){
			name = 'John';
			return "You are of the legal age";
		}

		function isUnderAge(){
			name = 'Jane';
			return "You are under age limit";
		}

		/*parseInt() - converts input received into a number data type*/
		let age = parseInt(prompt("What is your age?"));
		console.log(age);

		let legalAge = (age>=18) ? isOfLegalAge() : isUnderAge();
		console.log("Result of Ternary Operator in Functions: " + legalAge + ", " + name);



// [Section] Switch Statement

/*
	The switch statement evaluates an expression and matches the expression's value to a case claus. (case sensitive)

	Syntax:
		switch (expression) {
			case value: (double or single quote)
				statement;
				break; 
			default: (if statements ay hindi nameet sya ang mag ccatch)
				statement;
				break;
		}
*/
	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day){
		case 'monday':
			console.log("The color of the day is red");
			break;
		case 'tuesday':
			console.log("The color of the day is orange");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case "friday":
			console.log("The color of the day is blue");
			break;
		case "saturday":
			console.log("The color of the day is indigo");
			break;
		case "sunday":
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Please input a valid day");
			break;
	}


// [Section] Try-Catch-Finally Statement

/*	
	- "try-catch" statements are commonly use for error handling.
	- There are instances when the application returns an error or waring that is not necessarily an error in the context of our code.
	- These errors are result of an attempt of the programming languange to help developers in creating effecient codes.
	- They are used to specifi a response whenever an exception or error is received.
*/

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.warn(error.message);
		/*warn - yellow alert in console
		  .message - to show in the console*/
	}
	finally {
		alert("Intensity Updates will show alert.");
	}
}
showIntensityAlert(110);